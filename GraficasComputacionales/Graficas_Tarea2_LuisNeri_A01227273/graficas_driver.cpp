#include <GL\glew.h>
#include <GL\freeglut.h>
#include <iostream>
#include <stdio.h>

using namespace std;

// Global variables for window's size
int width = 800;
int height = 600;
int valor;
int apariencia = 0;
int slice = 50;
int stacks = 50;
int Rojo = 255;
int azul = 255;
int verde = 255;
// Global variables for moving camera
int mouseButton = 0;
bool mouseRotate = false;
bool mouseZoom = false;
bool mousePan = false;
int xClick = 0;
int yClick = 0;
int xAux = 0;
int yAux = 0;
double rotX = 0.;
double rotY = 0.;
double panX = 0.;
double panY = 0.;
double zoomZ = -5.;

void init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glColor3f(1.0, 1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0., zoomZ); // Camera inicialization
}

void reshape(int w, int h)
{
	width = w;
	height = h;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (height == 0)
	{
		height = 1;
	}
	gluPerspective(25.0, (double)width / height, 0.1, 1000.0);	//proyección en perspectiva
	glMatrixMode(GL_MODELVIEW);
}

void drawAxis(double length)
{
	glColor3f(1., 0., 0.);
	glBegin(GL_LINES);
	glVertex3f(0., 0., 0.);
	glVertex3f(length, 0., 0.);
	glEnd();
	glColor3f(0., 1., 0.);
	glBegin(GL_LINES);
	glVertex3f(0., 0., 0.);
	glVertex3f(0., length, 0.);
	glEnd();
	glColor3f(0., 0., 1.);
	glBegin(GL_LINES);
	glVertex3f(0., 0., 0.);
	glVertex3f(0., 0., length);
	glEnd();
	glColor3f(1., 1., 1.);
}


void idle()
{
	glutPostRedisplay();    // call display
}

void mouse(int button, int state, int x, int y)
{
	mouseButton = button;
	mouseRotate = false;
	mouseZoom = false;
	mousePan = false;
	xClick = x;
	yClick = y;
	if (button == GLUT_LEFT_BUTTON)
	{
		mouseRotate = true;
		xAux = rotX;
		yAux = rotY;
	}
	else if (button == GLUT_RIGHT_BUTTON)
	{
		mouseZoom = true;
		xAux = zoomZ;
	}
	else if (button == GLUT_MIDDLE_BUTTON)
	{
		mousePan = true;
		xAux = panX;
		yAux = panY;
	}
}

void mouseMotion(int x, int y)
{
	if (mouseRotate == true)
	{
		if (mouseButton == GLUT_LEFT_BUTTON)
		{
			if ((x - xClick + xAux) > 360.0)
			{
				xAux = xAux - 360.0;
			}
			if ((x - xClick + xAux) < 0.0)
			{
				xAux = xAux + 360.0;
			}
			if ((y - yClick + yAux) > 360.0)
			{
				yAux = yAux - 360.0;
			}
			if ((y - yClick + yAux) < 0.0)
			{
				yAux = yAux + 360.0;
			}
			rotX = x - xClick + xAux;
			rotY = y - yClick + yAux;
		}
	}
	else if (mouseZoom == true)
	{
		/*
		if (mouseButton == GLUT_RIGHT_BUTTON)
		{
			zoomZ = ((x - xClick) / 10.0) + xAux;
		}*/
	}
	else if (mousePan == true)
	{
		if (mouseButton == GLUT_MIDDLE_BUTTON)
		{
			panX = ((x - xClick) / 63.0) + xAux;
			panY = ((y - yClick) / (-63.0)) + yAux;
		}
	}
}

void keyword(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 's':
		if (slice > 5)
		{
			slice -= 5;
		}
		
		break;
	case 'a':
		slice += 5;
		break;
	case 'z':
		if (stacks > 5)
		{
			stacks -= 5;
		}
		break;
	case 'x':
		stacks += 5;
		break;
	}

}

void specialkeyword(int key, int x, int y)
{
	//cout << "Key = " << key << " ,x= " << x << ",y= " << y << endl;
}

//MEtodos Para Figuras

void DibujarEsfera()
{
	if (apariencia == 0)
	{
		glColor3f(Rojo, verde, azul);
		glutWireSphere(1., slice, stacks);
	}
	else if (apariencia == 1)
	{
		glColor3f(Rojo, verde, azul);
		glutSolidSphere(1., slice, stacks);
	}
}

void DibujarCono()
{
	if (apariencia == 0)
	{
		glColor3f(Rojo, verde, azul);
		glutWireCone(1., 2, slice, stacks);
	}
	else if (apariencia == 1)
	{
		glColor3f(Rojo, verde, azul);
		glutSolidCone(1., 2, slice, stacks);
	}
}

void DibujarCubo()
{
	if (apariencia == 0)
	{
		glColor3f(Rojo, verde, azul);
		glutWireCube(2.);
	}
	else if (apariencia == 1)
	{
		glColor3f(Rojo, verde, azul);
		glutSolidCube(2.);
	}
}

void DibujarToroide()
{
	if (apariencia == 0)
	{
		glutWireTorus(.5, 1., slice, stacks);
	}
	else if (apariencia == 1)
	{
	}
}

void DibujarTetra()
{

}

void DibujarOcta()
{

}

void DibujarDode()
{

}

void DibujarIcos()
{

}

void DibujarTetera()
{

}

void DibujarPunto()
{

}

void DibujarLine()
{

}

void DibujarLineS()
{

}

void DibujarLineL()
{

}

void DibujarTriangulo()
{

}

void DibujarTrianguloS()
{

}

void DibujarTrianguloF()
{

}

void DibujarQuad()
{

}

void DibujarQuadS()
{

}

void DibujarPoligono()
{

}

void DibujarPiramide()
{

}

void MenuFiguras(int value)
{
	valor = value;
}

void MenuColor(int value)
{
	switch (value)
	{
	case 30:
		Rojo = 255;
		verde = 0;
		azul = 0;
		break;
	case 31:
		Rojo = 0;
		verde = 0;
		azul = 255;
		break;
	case 32:
		Rojo = 0;
		verde = 255;
		azul = 0;
		break;
	case 33:
		Rojo = 255;
		verde = 255;
		azul = 0;
		break;
	case 34:
		Rojo = 255;
		verde = 0;
		azul = 255;
		break;
	case 35:
		Rojo = 0;
		verde = 255;
		azul = 255;
		break;
	case 36:
		Rojo = 139;
		verde = 0;
		azul = 255;
		break;
	case 37:
		Rojo = 230;
		verde = 95;
		azul = 0;
		break;
	case 38:
		apariencia = 0;
		break;
	case 39:
		apariencia = 1;
		break;
	}
}

void draw()
{
	switch (valor)
	{
	case 1:
		DibujarEsfera();
		break;
	case 2:
		DibujarCono();
		break;
	case 3:
		DibujarCubo();
		break;
	case 4:
		DibujarToroide();
		break;
	case 5:
		break;
	case 6:
		break;
	case 7:
		break;
	case 8:
		break;
	case 9:
		break;
	case 10:
		break;
	case 11:
		break;
	case 12:
		break;
	case 13:
		break;
	case 14:
		break;
	case 15:
		break;
	case 16:
		break;
	case 17:
		break;
	case 18:
		break;
	case 19:
		break;
	case 20:
		break;
	case 21:
		break;
	case 22:
		break;

	}

}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Moving camera
	glPushMatrix();
	glTranslatef(panX, panY-.5, zoomZ);
	glRotatef(rotY, 1.0, 0.0, 0.0);
	glRotatef(rotX, 0.0, 1.0, 0.0);

	// Scene Model
	drawAxis(2.);       // Drawing global coordinate system
	draw();
	//End Scene Model

	glPopMatrix();
	glutSwapBuffers();	// Swap buffers
}

int main(int argc, char** argv)
{
	// Init Window (GLUT)
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutCreateWindow("3D Viewer");

	//Key
	glutKeyboardFunc(keyword);
	glutSpecialFunc(specialkeyword);

	//Creacion De sub-menu's y Menu Principal

	//Creacion de sub-menus OpenGL
	int creacionOpenGL = glutCreateMenu(MenuFiguras);
	glutAddMenuEntry("Esfera", 1);
	glutAddMenuEntry("Cono", 2);
	glutAddMenuEntry("Cubo", 3);
	glutAddMenuEntry("Toroide", 4);
	glutAddMenuEntry("Tetrahedro", 5);
	glutAddMenuEntry("Octahedro", 6);
	glutAddMenuEntry("Dodecahedro", 7);
	glutAddMenuEntry("Icosahedro", 8);
	glutAddMenuEntry("Tetra", 9);
	glutAddMenuEntry("Cilindro", 10);
	glutAddMenuEntry("Disco", 11);

	//Creacion sub-menu Glut
	int creacionGlut = glutCreateMenu(MenuFiguras);
	glutAddMenuEntry("Punto", 12);
	glutAddMenuEntry("Linea", 13);
	glutAddMenuEntry("Line Strip", 14);
	glutAddMenuEntry("Line Loop", 15);
	glutAddMenuEntry("Triangulo", 16);
	glutAddMenuEntry("Triangulo Strip", 17);
	glutAddMenuEntry("Triangulo Fan", 18);
	glutAddMenuEntry("Quad", 19);
	glutAddMenuEntry("Quad Strip", 20);
	glutAddMenuEntry("Poligono", 21);
	glutAddMenuEntry("Piramide", 22);

	//Color
	int color = glutCreateMenu(MenuColor);
	glutAddMenuEntry("Rojo", 30);
	glutAddMenuEntry("Azul", 31);
	glutAddMenuEntry("Verde", 32);
	glutAddMenuEntry("Amarillo", 33);
	glutAddMenuEntry("Magenta", 34);
	glutAddMenuEntry("Cian", 35);
	glutAddMenuEntry("Morado", 36);
	glutAddMenuEntry("Naranja", 37);

	//Apariencia
	int apariencia = glutCreateMenu(MenuColor);
	glutAddMenuEntry("WireFrame", 38);
	glutAddMenuEntry("Solid", 39);

	//Creacion sub-menu Edicion
	int edicion = glutCreateMenu(MenuFiguras);
	glutAddSubMenu("Color", color);
	glutAddSubMenu("Apariencia", apariencia);

	//Creacion Menu Principal
	int MenuPrincipal = glutCreateMenu(MenuFiguras);
	glutAddSubMenu("Creacion OpenGL", creacionOpenGL);
	glutAddSubMenu("Creacion Glut", creacionGlut);
	glutAddSubMenu("Edicion", edicion);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	// Callback functions registration (GLUT)
	glutDisplayFunc(display);		// display scene function
	glutReshapeFunc(reshape);		// reshape window function
	glutIdleFunc(idle);				// idle function
	glutMouseFunc(mouse);			// processing mouse events function
	glutMotionFunc(mouseMotion);	// processing mouse motion function

	




	init();							// init OpenGL context

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		fprintf(stderr, "GLEW error");
		return 1;
	}
	glutMainLoop();					// Main loop waiting for events!
	return 0;
}

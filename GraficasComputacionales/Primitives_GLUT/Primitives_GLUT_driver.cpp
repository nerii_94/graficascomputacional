#include <GL\glew.h>
#include <GL\freeglut.h>
#include <iostream>

using namespace std;

float zoom = -10.;

void init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glColor3f(1.0, 1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0., 0., zoom); // Camera/World initial position
}

void reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (h == 0)
	{
		h = 1;
	}
	gluPerspective(25.0, (double)w / h, 0.1, 1000.0);	//proyección en perspectiva
	glMatrixMode(GL_MODELVIEW);
}

void draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	glTranslatef(0., 0., zoom);
	
	glColor3f(1., 1., 1.);
	glPolygonMode(GL_FRONT, GL_LINE);
	glPolygonMode(GL_BACK, GL_FILL);

	glBegin(GL_POLYGON);
	glVertex3f(-1., 0., 0.);
	glVertex3f(1., 0., 0.);
	glVertex3f(0., 1., 0.);
	glEnd();

	glColor3f(1., 0., 0.);
	glBegin(GL_LINES);
	glVertex3f(0., 0., 0.);
	glVertex3f(1.5, 0., 0.);
	glEnd();

	glColor3f(1., 0., 1.);
	glBegin(GL_LINES);
	glVertex3f(0., 0., 0.);
	glVertex3f(0., 1.5, 0.);
	glEnd();
	
	//glutSolidTeapot(1);
	glutSwapBuffers();	// Swap buffers
}

void idle()
{
	printf("Idle...");
	glutPostRedisplay();    // call display
}

void keyboard(unsigned char key, int x, int y){
	//printf("Tecla %c \n", key);

	if (key == '+'){
		printf("Teclassssssssssss %f \n", zoom);
		zoom = zoom + .5;
	}
	glutPostRedisplay();

}

void SpecialKey(int key, int x, int y){
	printf("Tecla %i\n", x);
}

int main(int argc, char** argv)
{
	// Init Window (GLUT)
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow("3D Viewer");

	// Callback functions registration (GLUT)
	glutDisplayFunc(draw);		// display scene function
	glutReshapeFunc(reshape);		// reshape window function
	//glutIdleFunc(idle);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(SpecialKey);

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		fprintf(stderr, "GLEW error");
		return 1;
	}

	init();							// init OpenGL context

	glutMainLoop();					// Main loop waiting for events!
	return 0;
}
